module.exports = {
  anyMissingParam (payload) {
    const requiredFields = ['name', 'email', 'password', 'passwordConfirmation'];

    return requiredFields.find(field => !payload[field]);
  },

  anyMalformedFields (payload) {
    const emailRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*/;
    const passwordRegex = /\b[a-zA-Z0-9]{8,32}\b/;

    if (payload.name === '') return 'name';
    if (!payload.email.match(emailRegex)) return 'email';
    if (!payload.password.match(passwordRegex)) return 'password';

    return false;
  },

  passwordsMatch (payload) {
    return payload.password === payload.passwordConfirmation;
  }
}