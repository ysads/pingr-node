const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const validator =  require("./validator/user");
const MongoDB = require('mongodb')
const { v4: uuid } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();
  const db = mongoClient.db('ep1');

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (_, res) => res.json("Hello, World!"));

  api.post("/users", async (req, res) => {
    const params = req.body;
    const id = uuid();

    const missingParam = validator.anyMissingParam(params);
    if (missingParam) {
      return res.status(400).json({ error: `Request body had missing field ${missingParam}` });
    }

    const malformedParam = validator.anyMalformedFields(params);
    if (malformedParam) {
      return res.status(400).json({ error: `Request body had malformed field ${malformedParam}` });
    }

    const hasDuplicates = await db.collection('users').findOne({ email: params.email });
    if (hasDuplicates) {
      return res.status(400).json({ error: 'Request body had malformed field email' });
    }

    if (!validator.passwordsMatch(params)) {
      return res.status(422).json({ error: 'Password confirmation did not match' });
    }

    stanConn.publish('users', JSON.stringify({
      eventType: "UserCreated",
      entityId: id,
      entityAggregate: {
        name: params.password,
        email: params.email,
        password: params.password,
      }
    }))

    res.status(201).json({
      user: {
        id,
        email: params.email,
        name: params.name
      }
    });
  })

  api.delete("/users/:id", async (req, res) => {
    const id = req.params.id;

    if (!req.headers['authentication']) {
      return res.status(401).json({ error: 'Access Token not found' });
    }

    const [_, token] = req.headers['authentication'].split("Bearer ");
    const decodedToken = jwt.verify(token, secret);

    if (id !== decodedToken.id) {
      return res.status(403).json({ error: 'Access Token did not match User ID' });
    }

    await db.collection('users').deleteOne({ _id: new MongoDB.ObjectID(id) })

    stanConn.publish('users', JSON.stringify({
      eventType: "UserDeleted",
      entityId: id,
      entityAggregate: {}
    }))

    res.status(200).json({ id });
  })

  return api;
};
