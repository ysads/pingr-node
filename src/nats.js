const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    /* ******************* */
    /* YOUR CODE GOES HERE */
    /* ******************* */
    const opts = conn.subscriptionOptions().setDeliverAllAvailable();
    const subscription = conn.subscribe('users', opts);

    const eventStore = mongoClient.db('ep1').collection('events');

    subscription.on('message', (event) => {
      const message = JSON.parse(event.getData());
      eventStore.insertOne(message);
    });

    subscription.on('unsubscribed', () => conn.close());
  });

  return conn;
};
